package observer
import kotlin.properties.Delegates

class Carro {
    var observer: CarroColorObserver? = null

    var color: String by Delegates.observable(""){
            propiedad, valorAnterior, nuevoValor ->  observer?.onCambioColor(nuevoValor)
    }
}