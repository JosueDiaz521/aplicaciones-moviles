package observer

class LogCarroColorObserver : CarroColorObserver{
    override fun onCambioColor(nuevoColor: String) {
        println("El carro se cambio de color a $nuevoColor")
    }
}