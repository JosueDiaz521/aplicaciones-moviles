package decorator

interface ServicioCarroDecorator: ServicioCarro

class ServicioBasico: ServicioCarro{
    override fun ejecutarServicio() {
        println("Ejucatando Servicio Basico\nTerminado")
    }
}

class Llavado(private val servicioCarro: ServicioCarro): ServicioCarroDecorator{
    override fun ejecutarServicio() {
        servicioCarro.ejecutarServicio()
        println("Ejecutando llavado externo del carro\nTerminado")
    }
}

class LlavadoInterno(private val servicioCarro: ServicioCarro): ServicioCarroDecorator{
    override fun ejecutarServicio() {
        servicioCarro.ejecutarServicio()
        println("Ejecutando llavado interno\nTerminado")
    }
}