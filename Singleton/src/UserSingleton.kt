object UserSingleton {
    var nombre = ""

    init {
        println("Singleton invocado")
    }

    fun imprimir() = println(nombre)
}