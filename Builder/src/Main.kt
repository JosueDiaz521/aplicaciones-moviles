import builder.*

fun main(args: Array<String>){
    var compBuilder = ComputadorBuilder()
    compBuilder.setOS("Linux").setRAM(16).setTamanioPantalla(21.5).setTecladoExterno(true)

    var cpu1 = Computador(compBuilder)
    println(cpu1.build())
}