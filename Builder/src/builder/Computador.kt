package builder

class Computador {
    var cb: ComputadorBuilder
    constructor(computadorBuilder: ComputadorBuilder){
        this.cb = computadorBuilder
    }

    fun build():String{
        return ("La computadora tienes las siguientes caracteristicas :" +
                "\nOS : ${cb.getOS()} " +
                "\nRAM : ${cb.getRAM()} " +
                "\nTamaño Pantalla : ${cb.getTamanioPantalla()} " +
                "\nMouse Externo : ${cb.getMouseExterno()} " +
                "\nTeclado Externo : ${cb.getTecladoExterno()}"
                )
    }
}