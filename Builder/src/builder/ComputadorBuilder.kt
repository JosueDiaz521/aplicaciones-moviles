package builder

open class ComputadorBuilder {
    private var OS: String? = null
    private var RAM: Int? = null
    private var tamanioPantalla: Double? = null
    private var mouseExterno: Boolean? = null
    private var tecladoExterno: Boolean? = null

    fun setOS(OS:String): ComputadorBuilder{
        this.OS = OS
        return this
    }
    fun getOS():String?{
        return this.OS
    }
    fun setRAM(RAM:Int): ComputadorBuilder{
        this.RAM = RAM
        return this
    }
    fun getRAM():Int?{
        return this.RAM
    }
    fun setTamanioPantalla(tamanioPantalla:Double): ComputadorBuilder{
        this.tamanioPantalla = tamanioPantalla
        return this
    }
    fun getTamanioPantalla():Double?{
        return this.tamanioPantalla
    }
    fun setMouseExterno(mouseExterno:Boolean): ComputadorBuilder{
        this.mouseExterno = mouseExterno
        return this
    }
    fun getMouseExterno():Boolean?{
        return this.mouseExterno
    }

    fun setTecladoExterno(tecladoExterno:Boolean): ComputadorBuilder{
        this.tecladoExterno = tecladoExterno
        return this
    }
    fun getTecladoExterno():Boolean?{
        return this.tecladoExterno
    }
}