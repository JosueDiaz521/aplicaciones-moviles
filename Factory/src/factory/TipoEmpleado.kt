package factory

enum class TipoEmpleado { Mesero, Chef, Cocinero}

class Mesero(override val nombre: String, override val apellido: String) : Empleado{
    override fun cargo()="Mesero"
    override fun salario() = 325.00
}

class Chef(override val nombre: String, override val apellido: String) : Empleado{
    override fun cargo()="Chef"
    override fun salario() = 523.00
}

class Cocinero(override val nombre: String, override val apellido: String) : Empleado{
    override fun cargo()="Cocinero"
    override fun salario() = 365.00
}

