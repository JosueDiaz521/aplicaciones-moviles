package factory

object EmpleadoFactory {
    fun getEmpleado (tipoEmpleado: TipoEmpleado,nombre:String,apellido:String): Empleado{
        return when (tipoEmpleado){
            TipoEmpleado.Mesero -> Mesero  (nombre = nombre, apellido = apellido)
            TipoEmpleado.Chef -> Chef(nombre = nombre, apellido = apellido)
            TipoEmpleado.Cocinero -> Cocinero(nombre = nombre, apellido = apellido)
        }
    }
}