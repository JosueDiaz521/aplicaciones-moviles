package factory

interface Empleado {
    val nombre: String
    val apellido: String
    fun cargo(): String
    fun salario(): Double
    fun getNombreCompleto() = "\n$nombre $apellido"
}