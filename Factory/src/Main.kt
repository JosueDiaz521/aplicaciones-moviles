import factory.*

fun main() {
    val cocinero = EmpleadoFactory.getEmpleado(TipoEmpleado.Cocinero, "Andres", "Mendez")
    with(cocinero) {
        println(getNombreCompleto())
        println(cargo())
        println("Sueldo : ${salario()}")
    }

    val chef = EmpleadoFactory.getEmpleado(TipoEmpleado.Chef, "Mathias", "Ruales")
    with(chef) {
        println(getNombreCompleto())
        println(cargo())
        println("Sueldo : ${salario()}")
    }

    val mesero = EmpleadoFactory.getEmpleado(TipoEmpleado.Mesero, "Erick", "Acosta")
    with(mesero) {
        println(getNombreCompleto())
        println(cargo())
        println("Sueldo : ${salario()}")
    }
}